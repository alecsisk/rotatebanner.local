## Описание
Техзадание, выполнено по данным файла Тестовое задание.pdf


## Как развернуть проект
- vagrant box add laravel/homestead
- переходим в корень репозитория, он же корень проекта
- composer install
- php vendor/bin/homestead make
- возможно нужно поменять виртуальную машину в файле Homestead.yaml
- vagrant up
- sudo vi /etc/hosts, добавляем строку:
192.168.10.10   rotatebanner.local
- vagrant ssh
- cd code
- php artisan migrate
- php artisan db:seed
- Готово. Сайт работает на http://rotatebanner.local. Данные для входа admin@admin.com:secret 
