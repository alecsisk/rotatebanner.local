<?php

use Illuminate\Database\Seeder;

class BannersTableSeeder extends Seeder {


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run () {

        // создаем баннеры с разными приоритетами
        for ($i = 1; $i <= 3; $i++) {
            factory(App\Banner::class, 'banner', 1)->create(['priority' => $i, 'code' => '(function (w, d, lib) { var sId = lib + \'_script\'; var init = function () {w[lib].init(' . $i . ', {containerId: \'container\', onEmpty: function () {loadNextBanner();},onSuccess: function () {/** Обработчик успешного показа */} });};if (d.getElementById(sId) === null) { var s = d.createElement(\'script\'); s.id = sId;s.onload = init;s.src = \'http://test.dev.justlady.ru/lib.js?s=\' + Date.now();d.head.appendChild(s); } else init();})(window, document, \'AdLib\');']);
        }
    }
}
