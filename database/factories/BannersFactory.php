<?php

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Model;


$factory->defineAs(App\Banner::class, 'banner', function (Faker $faker) {

    return [
        'name' => $faker->text(15)
    ];
});
