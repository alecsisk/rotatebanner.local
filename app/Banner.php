<?php

namespace App;

use App\Utils\ArrayUtil;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model {
    public $timestamps = false;
    public $fillable = ['name', 'code', 'priority'];


    /**
     * получить разрешенные приоритеты
     */
    public static function getAllowPriorities ($method) {

        // все приоритеты
        $priorities = array_column(Banner::all('id', 'priority')->sortByDesc('priority')->toArray(), 'priority');


        // если есть приоритеты
        if (count($priorities) > 0) {


            // если создание
            if ($method == 'create') {

                // добавляем еще один приоритет, самый высокий, либо баннер заменит по приоритету другой
                $priorities = array_merge([count($priorities) + 1 => count($priorities) + 1], $priorities);
            }

        } else {

            // если нет ни одного баннера, то доступен только один приоритет
            array_push($priorities, 1);
        }


        // ключи как значения
        $priorities = ArrayUtil::valuesToKeys($priorities);

        // возвращаем приоритеты
        return $priorities;
    }
}
