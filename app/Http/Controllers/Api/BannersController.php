<?php

namespace App\Http\Controllers\Api;

use App\Banner;
use App\Http\Controllers\Controller;

/**
 * Created by IntelliJ IDEA.
 * User: vision
 * Date: 03.09.2018
 * Time: 19:28
 */

class BannersController extends Controller {


    public function index () {
        return Banner::all();
    }
}
