<?php

namespace App\Http\Controllers;

use App;
use App\Banner;
use App\Http\Requests\CreateBannerRequest;
use App\Http\Requests\UpdateBannerRequest;

class BannersController extends Controller {


    public function __construct () {
        $this->middleware('auth');
    }


    public function index () {
        $banners = Banner::all()->sortByDesc('priority');
        return view('banners.index', compact('banners'));
    }


    public function show ($id) {
        $banner = Banner::find($id);
        $priorities = Banner::getAllowPriorities('update');

        return view('banners.edit', compact(['banner', 'priorities']));
    }


    public function create () {
        $priorities = Banner::getAllowPriorities('create');
        return view('banners.create', compact('priorities'));
    }


    public function store (CreateBannerRequest $request) {

        // приоритет баннера
        $priority = $request->get('priority');

        // перестройка приоритетов
        Banner::where('priority', '>=', $priority)->increment('priority', 1);

        // сохраняем
        $banner = new Banner();
        $banner->fill($request->all());
        $banner->save();

        // редиректим на индексную
        return redirect()->route('banners.index');
    }


    public function update ($id, UpdateBannerRequest $request) {

        // находим баннер
        $banner = Banner::findOrFail($id);

        // текущий приоритет баннера
        $currentPriority = $banner->priority;

        // приоритет, который нужно установить баннеру
        $priority = $request->get('priority');

        // если новый приоритет > текущий
        if ($priority > $currentPriority) {

            // все приоритеты от текущего до нового сдвигаются на 1 в сторону меньше
            Banner::where('priority', '<=', $priority)->where('priority', '>', $currentPriority)->decrement('priority', 1);

        } else if ($priority < $currentPriority) {

            // все приоритеты от текущего до нового сдвигаются на 1 в сторону больше
            Banner::where('priority', '>=', $priority)->where('priority', '<', $currentPriority)->increment('priority', 1);

        }

        // сохраняем данные
        $banner->fill($request->all());
        $banner->save();

        // редиректим на главную
        return redirect()->route('banners.index');
    }


    public function destroy ($id) {

        // удаляем данные
        $banner = Banner::findOrFail($id);

        // перестройка приоритетов
        Banner::where('priority', '>', $banner->priority)->decrement('priority', 1);

        // удаляем баннер
        $banner->delete();

        // редиректим на главную
        return redirect()->route('banners.index');
    }
}
