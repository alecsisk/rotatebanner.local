<?php


namespace App\Utils;


class ArrayUtil {


    /**
     * ключи массива как значения массива
     * @param $arr
     * @return array
     */
    public static function valuesToKeys ($arr) {

        $result = [];

        foreach ($arr as $item) {

            $result[$item] = $item;
        }

        return $result;
    }
}
