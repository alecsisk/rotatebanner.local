@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">

                    <div class="card-header">Добавление рекламного кода</div>

                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="card-body">
                        {!! Form::open(['route' => ['banners.store'], 'method' => 'POST']) !!}
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                {{ Form::label('name', 'Название') }}
                                {{ Form::text('name', '', ['class'=>'form-control']) }}
                            </div>
                            <div class="col-md-6 mb-3">
                                {{ Form::label('priority', 'Приоритет') }}
                                {{ Form::select('priority', $priorities, max(array_keys($priorities)), ['class'=>'form-control']) }}
                            </div>
                        </div>

                        {{ Form::label('code', 'Рекламный JS-код') }}
                        {{ Form::textarea('code', '', ['class'=>'form-control']) }}
                        <div class="modal-footer">
                            <button id="cancel" type="button" data-url="{{ route('banners.index') }}" class="btn btn-danger" href="#">
                                Отмена
                            </button>
                            <button class="btn btn-success" type="submit">Сохранить</button>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

        $(document).ready(function () {

            $('#cancel').click(function () {
                window.location.href = $(this).data('url');
            });
        });
    </script>

@endsection
