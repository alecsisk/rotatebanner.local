@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row justify-content-center">

            <div class="col-md-8">

                <div class="card">
                    <div class="card-header" style="display: table; width: 100%">
                        <div style="display: table-cell;">Список рекламных кодов</div>
                        <div style="display: table-cell; text-align: right">
                            {!! Form::open(['route' => ['banners.create'], 'method'=> 'GET']) !!}
                            <button type="submit" class="btn btn-primary">Добавить</button>
                            {!! Form::close() !!}
                        </div>

                    </div>

                    <div class="card-body">
                        @foreach($banners as $banner)

                            <div class="row" style="border-bottom: 1px solid #d8d8d8">
                                <div class="col-md-1 color-blue">{{$banner->priority}}</div>
                                <div class="col-md-7">{{$banner->name}}</div>

                                <div class="col-md-4">
                                    <a class="a-edit color-red removeBanner"
                                       data-token="{{ csrf_token() }}"
                                       data-url="{{ route('banners.destroy', ['id' => $banner->id]) }}"
                                       href="#">Удалить</a>
                                    <a class="a-edit"
                                       href="{{ route('banners.show', ['id' => $banner->id]) }}">Редактировать</a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

        $(document).ready(function () {

            /**
             * удалить баннер через ajax
             */
            $('.removeBanner').click(function () {
                var url = $(this).data("url");
                var token = $(this).data("token");

                $.ajax({
                    'url': url,
                    'type': 'POST',
                    'data': { _method: 'delete', _token: token },
                    'success': function () {
                        location.reload();
                    }
                });
            })
        });

    </script>

@endsection
