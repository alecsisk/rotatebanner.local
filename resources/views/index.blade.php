@extends('layouts.app')

@section('content')

    <div id="container">

    </div>

    <input id="route-banner" type="hidden" data-url="{{ route('api.banners.index') }}">


    <script>

        $(document).ready(function () {

            // список баннеров для показа
            var banners = null;


            $.ajax({
                'url': $('#route-banner').data('url'),
                'type': 'GET',
                'success': function (data) {

                    /**
                     * сортировка
                     */
                    function compare(a, b) {
                        if (a.priority > b.priority)
                            return -1;
                        if (a.priority < b.priority)
                            return 1;
                        return 0;
                    }

                    // сортируем по приоритету
                    banners = data.sort(compare);

                    // загружаем баннер для показа
                    loadNextBanner();

                },
                'fail': function () {
                    console.log('ajax load failed');
                }
            });


            /**
             * загружаем следующий баннер
             */
            function loadNextBanner () {

                // если есть баннеры для показа
                if (banners.length > 0) {

                    // удаляем первый элемент и получаем его данные
                    var banner = banners.splice(0, 1)[0];

                    // код для подключения
                    var code = banner['code'];

                    // выполняем код для показа
                    eval(code);

                } else {

                    console.log('banners has not found');
                }
            }
        });

    </script>

@endsection
